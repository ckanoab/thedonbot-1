from datetime import datetime, timedelta
from time import mktime
import plugins

def _initialise(bot):
    plugins.register_user_command(["checkpoint"]); 

def checkpoint(bot, event, *args):
    if args:
        count = args[0]
    else:
        count = 5
    

    t0 = datetime.strptime('2016-02-05 11', '%Y-%m-%d %H')
    hours_per_cycle = 175
    
        
    t = datetime.now()

    seconds = mktime(t.timetuple()) - mktime(t0.timetuple())
    cycles = seconds // (3600 * hours_per_cycle)
    start = t0 + timedelta(hours=cycles * hours_per_cycle)
    checkpoints = map(lambda x: start + timedelta(hours=x), range(0, hours_per_cycle, 5))

    
    s = "<i><b>Next %s checkpoints</b></i> \n" % str(count)
    x = 1

    for num, checkpoint in enumerate(checkpoints):
        if checkpoint > t:
            s+= '<i>%s EST<i> \n' % (checkpoint.strftime("%a %d, X%I %p").replace('X0', 'X').replace('X',''))
            x+=1
        if x == int(count)+1:
            break
    s+= "\n"

    s+= '<b>Septicycle Ends</b>: %s EST' % (max(checkpoints).strftime("%a %d, X%I %p").replace('X0', 'X').replace('X',''))

    yield from bot.coro_send_message(event.conv, s)
